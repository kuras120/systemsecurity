call venv\Scripts\activate.bat
echo.
echo Vernam-Cipher tests
python -m unittest test/test_lab2.py
echo.
echo RSA-Cipher tests
python -m unittest test/test_lab3.py
echo.
echo SHA256 tests
python -m unittest test/test_lab4.py
echo.
pause
cls
python main.py
call venv\Scripts\deactivate.bat
echo.
pause
