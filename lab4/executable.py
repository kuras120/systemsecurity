from lab4.sha256_hash import SHA256


def run():
    file_path = input('Enter file path (e.g. test/test_lab4): ')
    with open(file_path, 'r', encoding='utf-8') as file:
        plain_text = file.read()
        print('\nPlain text: ', plain_text)
        encrypted = SHA256.encode(plain_text)
        print('Encrypted value: ', encrypted.encode('utf-8', 'replace').decode())
