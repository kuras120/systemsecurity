import os
import sys
import hashlib

from utils.data_reader import DataReader


class SHA256:
    def __init__(self):
        self.__values = DataReader.read_sha_data(os.path.join(sys.path[0], 'lab4/data/hash_values'))
        self.__constants = DataReader.read_sha_data(os.path.join(sys.path[0], 'lab4/data/hash_constants'))

    @staticmethod
    def encode(text: str):
        sha3_256 = hashlib.sha3_256()
        sha3_256.update(text.encode('utf-8'))
        return sha3_256.hexdigest()
