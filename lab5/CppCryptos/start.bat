@echo off
echo Utf-8 activation
echo.
chcp 65001
echo.
echo Start AES-256 cipher
echo.
botan-install\bin\botan-cli.exe cipher --cipher=AES-256/CTR --key=5a6e52544e6968684d4161634258436c726d34566d354542514e734846303356 test.txt > aes_cipher.txt
type aes_cipher.txt
echo.
botan-install\bin\botan-cli.exe cipher --cipher=AES-256/CTR --decrypt --key=5a6e52544e6968684d4161634258436c726d34566d354542514e734846303356 aes_cipher.txt
echo.
echo.
echo Start TripleDES cipher
echo.
botan-install\bin\botan-cli.exe cipher --cipher=TripleDES/CBC/PKCS7 --key=6b6e54665034337665455832796d64754f71696d45724735 test.txt > des_cipher.txt
type des_cipher.txt
echo.
botan-install\bin\botan-cli.exe cipher --cipher=TripleDES/CBC/PKCS7 --decrypt --key=6b6e54665034337665455832796d64754f71696d45724735 des_cipher.txt
echo.
echo.
echo Generating RSA keys
echo.
botan-install\bin\botan-cli.exe keygen --algo=RSA > private_key.pem
botan-install\bin\botan-cli.exe pkcs8 --pub-out private_key.pem > public_key.pem
echo Start aead AES-256 (RSA) cipher
echo.
botan-install\bin\botan-cli.exe pk_encrypt --aead=AES-256/GCM public_key.pem test.txt > rsa1_cipher.txt
type rsa1_cipher.txt
echo.
botan-install\bin\botan-cli.exe pk_decrypt private_key.pem rsa1_cipher.txt
echo.
echo.
echo Start aead ChaCha20Poly1305 cipher
echo.
botan-install\bin\botan-cli.exe pk_encrypt --aead=ChaCha20Poly1305 public_key.pem test.txt > rsa2_cipher.txt
type rsa2_cipher.txt
echo.
botan-install\bin\botan-cli.exe pk_decrypt private_key.pem rsa2_cipher.txt
echo.
echo.
echo Start SHA-256 hashing
echo.
botan-install\bin\botan-cli.exe hash --algo=SHA-256 --no-fsname test.txt
echo.
echo Start Whirlpool hashing
botan-install\bin\botan-cli.exe hash --algo=Whirlpool --no-fsname test.txt
echo.
echo Start SHA-256 signing
echo.
botan-install\bin\botan-cli.exe sign --hash=SHA-256 private_key.pem test.txt > sign.txt
type sign.txt
echo.
botan-install\bin\botan-cli.exe verify --hash=SHA-256 public_key.pem test.txt sign.txt
echo.
pause
