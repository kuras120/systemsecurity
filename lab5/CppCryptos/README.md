#CppCryptos

---
1. Uruchamianie testow szyfrowania/hashowania za pomoca start.bat
2. Czasy:
- CTR-BE(AES-256) ran 804 tests in 5.82 msec all ok
- TripleDES/CBC/PKCS7 ran 37 tests in 0.25 msec all ok

- AES-256/GCM ran 736 tests in 3.92 msec all ok (RSA)
- ChaCha20Poly1305 ran 41587 tests in 231.19 msec all ok (RSA)

- SHA-256 ran 11679 tests in 11.39 msec all ok
- Whirlpool ran 73 tests in 0.11 msec all ok
 
 #Ocena

 ---
 - Biblioteka nie wymaga znajomości jezyka C++, jedynie konfiguracji programu przez skrypt pythonowy oraz jej kompilacji
 - Test szyfrowania/hashowania wykonany z pomoca https://botan.randombit.net/handbook/cli.html, a czasy wyliczone podczas kompilacji
 - Szybkosc na poziomie pojedynczych milisekund, jest wiec szybszy od BouncyCastle (C++ dziala niskopoziomowo)
 - Brak listy dostepnych permutacji algorytmow, paddingow itp. Jedyne wyjscie to sprawdzenie testow, ktore pokrywaja kazda dostepna opcje