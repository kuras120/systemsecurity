package crypto;

import model.Parameters;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Base64;

public class Symmetric {

  private static final SecureRandom RANDOM = new SecureRandom();

  private static IvParameterSpec padding;
  private static SecretKey key;

  public static byte [] encrypt(String plaintext, Parameters type) throws Exception {
    String cipherText = type.getCipher();
    key = generateKey(type);
    Cipher cipher = Cipher.getInstance(cipherText);
    padding = generateIV(cipher);
    cipher.init(Cipher.ENCRYPT_MODE, key, padding);
    System.out.println("Key: " + Base64.getEncoder().withoutPadding().encodeToString(key.getEncoded()));
    return cipher.doFinal(plaintext.getBytes(StandardCharsets.UTF_8));
  }

  public static String decrypt(byte [] ciphertext, Parameters type) throws Exception {
    String cipherText = type.getCipher();
    Cipher cipher = Cipher.getInstance(cipherText);
    cipher.init(Cipher.DECRYPT_MODE, key, padding);
    return new String(cipher.doFinal(ciphertext), StandardCharsets.UTF_8);
  }

  private static SecretKey generateKey(Parameters type) {
    byte[] keyBytes = new byte[type.getKeySize()];
    RANDOM.nextBytes(keyBytes);
    return new SecretKeySpec(keyBytes, type.getCipher().split("/")[0]);
  }

  private static IvParameterSpec generateIV(Cipher cipher) {
    byte [] ivBytes = new byte[cipher.getBlockSize()];
    RANDOM.nextBytes(ivBytes);
    return new IvParameterSpec(ivBytes);
  }
}
