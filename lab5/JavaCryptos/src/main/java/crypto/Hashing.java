package crypto;

import model.Parameters;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class Hashing {

  public static byte[] encrypt(String plaintext, Parameters type) throws Exception {
    MessageDigest md = MessageDigest.getInstance(type.getCipher());
    return md.digest(plaintext.getBytes(StandardCharsets.UTF_8));
  }
}
