package crypto;

import model.Parameters;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;
import java.util.Base64;

public class Asymmetric {

  private static KeyPair keys;

  public static byte[] encrypt(String plaintext, Parameters type) throws Exception {
    keys = generateKeys(type);
    Cipher cipher = Cipher.getInstance(type.getCipher());
    cipher.init(Cipher.ENCRYPT_MODE, keys.getPublic());
    byte[] plainBytes = plaintext.getBytes(StandardCharsets.UTF_8);
    return cipher.doFinal(plainBytes, 0, plainBytes.length);
  }

  public static String decrypt(byte[] ciphertext, Parameters type) throws Exception {
    String cipherText = type.getCipher();
    Cipher cipher = Cipher.getInstance(cipherText);
    cipher.init(Cipher.DECRYPT_MODE, keys.getPrivate());
    return new String(cipher.doFinal(ciphertext), StandardCharsets.UTF_8);
  }

  private static KeyPair generateKeys(Parameters type) throws NoSuchAlgorithmException {
    KeyPairGenerator generator = KeyPairGenerator.getInstance(type.getCipher().split("/")[0]);
    return generator.generateKeyPair();
  }
}
