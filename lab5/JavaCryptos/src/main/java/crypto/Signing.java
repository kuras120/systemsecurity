package crypto;

import model.Parameters;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;

public class Signing {

  private static KeyPair keys;

  public static byte[] sign(String plaintext, Parameters type) throws Exception {
    keys = generateKeys(type);
    Signature sign = Signature.getInstance(type.getCipher());
    sign.initSign(keys.getPrivate());
    sign.update(plaintext.getBytes("UTF-8"));
    return sign.sign();
  }

  public static boolean verify(String plaintext, byte[] signature) throws Exception {
    Signature verify = Signature.getInstance("SHA256withRSA", "BC");
    verify.initVerify(keys.getPublic());
    verify.update(plaintext.getBytes("UTF-8"));
    return verify.verify(signature);
  }

  private static KeyPair generateKeys(Parameters type) throws NoSuchAlgorithmException {
    KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
    return generator.generateKeyPair();
  }
}
