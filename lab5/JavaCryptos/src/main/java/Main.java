import crypto.Asymmetric;
import crypto.Hashing;
import crypto.Signing;
import crypto.Symmetric;
import model.Parameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.nio.file.Files;
import java.nio.file.Path;
import java.security.Provider;
import java.security.Security;
import java.util.*;

public class Main {

  private static final List<Parameters> symmetric = Arrays.asList(
      new Parameters("DESede/CBC/PKCS5Padding", 24),
      new Parameters("AES/CTR/TBCPadding", 32)
  );

  private static final List<Parameters> asymmetric = Arrays.asList(
      new Parameters("RSA/NONE/PKCS1Padding", 256),
      new Parameters("ElGamal/NONE/OAEPPadding", 256)
  );

  private static final List<Parameters> diggest = Arrays.asList(
      new Parameters("SHA3-512", 0),
      new Parameters("MD5", 0)
  );

  private static final List<Parameters> signature = Arrays.asList(
      new Parameters("SHA256withRSA", 0)
  );

  public static void main( String[] args ) throws Exception {
    Security.addProvider(new BouncyCastleProvider());
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter file path:\n");
    String filePath = scanner.next();
    String plainText, decryptedMessage;
    byte[] encryptedMessage;
    long startTime, endTime;
    int number;
    do {
      System.out.println("\nChoose algo:\n1. Symmetric\n2. Asymmetric\n3. Hashing\n4. Signing\n0. Quit\n");
      number = scanner.nextInt();
      switch (number) {
        case 1:
          System.out.println("\nChoose method:\n1. " + symmetric.get(0) + "\n2. " + symmetric.get(1) + "\n");
          number = scanner.nextInt();
          plainText = Files.readString(Path.of(filePath));
          System.out.println("Read from file: " + plainText);
          startTime = System.currentTimeMillis();
          encryptedMessage = Symmetric.encrypt(plainText, symmetric.get(number - 1));
          endTime = System.currentTimeMillis();
          System.out.println("Encryption time: " + (endTime - startTime) + " ms");
          System.out.println("Encrypted message: " + Base64.getEncoder().withoutPadding().encodeToString(encryptedMessage));
          startTime = System.currentTimeMillis();
          decryptedMessage = Symmetric.decrypt(encryptedMessage, symmetric.get(number - 1));
          endTime = System.currentTimeMillis();
          System.out.println("Decryption time: " + (endTime - startTime) + " ms");
          System.out.println("Decrypted message: " + decryptedMessage);
          break;
        case 2:
          System.out.println("\nChoose method:\n1. " + asymmetric.get(0) + "\n2. " + asymmetric.get(1) + "\n");
          number = scanner.nextInt();
          plainText = Files.readString(Path.of(filePath));
          System.out.println("Read from file: " + plainText);
          startTime = System.currentTimeMillis();
          encryptedMessage = Asymmetric.encrypt(plainText, asymmetric.get(number - 1));
          endTime = System.currentTimeMillis();
          System.out.println("Encryption time: " + (endTime - startTime) + " ms");
          System.out.println("Encrypted message: " + Base64.getEncoder().withoutPadding().encodeToString(encryptedMessage));
          startTime = System.currentTimeMillis();
          decryptedMessage = Asymmetric.decrypt(encryptedMessage, asymmetric.get(number - 1));
          endTime = System.currentTimeMillis();
          System.out.println("Decryption time: " + (endTime - startTime) + " ms");
          System.out.println("Decrypted message: " + decryptedMessage);
          break;
        case 3:
          System.out.println("\nChoose method:\n1. " + diggest.get(0) + "\n2. " + diggest.get(1) + "\n");
          number = scanner.nextInt();
          plainText = Files.readString(Path.of(filePath));
          System.out.println("Read from file: " + plainText);
          startTime = System.currentTimeMillis();
          encryptedMessage = Hashing.encrypt(plainText, diggest.get(number - 1));
          endTime = System.currentTimeMillis();
          System.out.println("Encryption time: " + (endTime - startTime) + " ms");
          System.out.println("Encrypted message: " + Base64.getEncoder().withoutPadding().encodeToString(encryptedMessage));
          break;
        case 4:
          System.out.println("\nChoose method:\n1. " + signature.get(0) + "\n");
          number = scanner.nextInt();
          plainText = Files.readString(Path.of(filePath));
          System.out.println("Read from file: " + plainText);
          startTime = System.currentTimeMillis();
          encryptedMessage = Signing.sign(plainText, signature.get(number - 1));
          endTime = System.currentTimeMillis();
          System.out.println("Signing time: " + (endTime - startTime) + " ms");
          System.out.println("Signed message: " + Base64.getEncoder().withoutPadding().encodeToString(encryptedMessage));
          startTime = System.currentTimeMillis();
          boolean verify = Signing.verify(plainText, encryptedMessage);
          endTime = System.currentTimeMillis();
          System.out.println("Verifying time: " + (endTime - startTime) + " ms");
          System.out.println("Verify message: " + verify);
          break;
      }
    } while (number != 0);
  }
}
