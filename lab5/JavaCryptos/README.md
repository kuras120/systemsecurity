#JavaCryptos

---
1. W celu uruchomienia: java -jar target/JavaCryptos-1.0-jar-with-dependencies.jar (>=java 11)
2. W celu zbudowania: mvn clean install
3. Po uruchomieniu nalezy podac sciezke do pliku z tekstem

#Ocena

---
- Biblioteka prosta i generyczna (można prosto stworzyc automat do testowania roznych algorytmow z roznymi paddingami)
- Test wykonany jedynie z pomoca https://www.bouncycastle.org/specifications.html, wiec niewielki prog wejscia
- Java jest bardziej przyjazna od niskopoziomych jezykow
- Szybkosc jest wystarczajaca, rzedu max setek milisekund, jednak wolniej od biblioteki Botan

