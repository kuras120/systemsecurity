import math

from Crypto.Util import number


class RSA:
    def __init__(self, rsa_key_length: int, block_size=10):
        self.__base = rsa_key_length
        self.__block_size = block_size
        self.__key = self.__generate_key(
            number.getPrime(rsa_key_length),
            number.getPrime(rsa_key_length)
        )

    def encode(self, text: str):
        strings = []
        print()
        for block in self.__make_blocks(text):
            # print('block ( base', self.__base, '): ', block, '\nk: ', self.__key[1], '\nn: ', self.__key[0])
            calculated = pow(block, self.__key[1], self.__key[0])
            print('Calculated cipher block: ', calculated)
            strings.append(self.__make_string(calculated))
        return strings

    def decode(self, crypto_blocks):
        strings = []
        print()
        for block in self.__make_blocks(crypto_blocks, True):
            # print('block ( base', self.__base, '): ', block, '\nd: ', self.__key[2], '\nn: ', self.__key[0])
            calculated = pow(block, self.__key[2], self.__key[0])
            print('Calculated text block: ', calculated)
            strings.append(self.__make_string(calculated))
        return strings

    def get_key(self):
        return self.__key

    def __make_blocks(self, text, decode=False):
        if not decode:
            blocks = [text[i:i + self.__block_size] for i in range(0, len(text), self.__block_size)]
            ascii_blocks = []
            for block in blocks:
                num, power = 0, 0
                for char in block:
                    num += ord(char) * pow(self.__base, power)
                    power += 1
                ascii_blocks.append(num)
            return ascii_blocks
        else:
            blocks = []
            for block in text:
                reversed_block = list(reversed(list(block)))
                integer = 0
                for num in reversed_block:
                    integer = ord(num) + (integer * self.__base)
                blocks.append(integer)
            return blocks

    def __make_string(self, block: int) -> str:
        string = ''
        integer = block
        while integer != 0:
            string += chr(integer % self.__base)
            integer = integer // self.__base
        return string

    @staticmethod
    def __generate_key(p: int, q: int) -> (int, int, int):
        n = p * q
        v = (p - 1) * (q - 1)
        k = RSA.__small_odd_int(v)
        d = pow(k, -1, v)
        return n, k, d

    @staticmethod
    def __small_odd_int(v: int) -> int:
        for num in range(2, v):
            if math.gcd(num, v) == 1:
                return num
