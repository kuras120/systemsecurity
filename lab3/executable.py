from lab3.rsa_cipher import RSA


def run():
    file_path = input('Enter file path (e.g. test/test_lab3): ')
    rsa = RSA(768)
    with open(file_path, 'r', encoding='utf-8') as file:
        plain_text = file.read()
        print('\nPlain text: ', plain_text)
        encrypted = rsa.encode(plain_text)
        encrypted_to_print = [element.encode('utf-8', 'replace').decode() for element in encrypted]
        print('Encrypted value: ', ''.join(encrypted_to_print))
        decrypted = rsa.decode(encrypted)
        print('Decrypted value: ', ''.join(decrypted))
