import os
import sys
import unittest2

from lab2.vernam_cipher import Vernam
from utils.bit_array import BitArray


class VernamCipherTest(unittest2.TestCase):
    def test_crypto(self):
        vernam = Vernam()
        with open(os.path.join(sys.path[0], 'test/test_lab2'), 'r', encoding='utf-8') as file:
            plain_text = file.read()
            encrypted = vernam.encode(plain_text)
            decrypted = vernam.decode(encrypted)
            self.assertEqual(plain_text, decrypted)

    def test_key_length(self):
        vernam = Vernam()
        with open(os.path.join(sys.path[0], 'test/test_lab2'), 'r', encoding='utf-8') as file:
            plain_text = file.read()
            encrypted = vernam.encode(plain_text)
            encrypted_bits = BitArray.to_bits(encrypted, 16)
            self.assertTrue(len(encrypted_bits) == len(vernam.get_key()))
