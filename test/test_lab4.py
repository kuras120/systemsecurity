import os
import sys
import math
import random
import string
import secrets
import unittest2

from lab4.sha256_hash import SHA256
from utils.bit_array import BitArray


class SHA256Test(unittest2.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.n = 1000
        if len(sys.argv) >= 3:
            cls.n = int(sys.argv[2])

    # 1. Czy dla losowego ciagu wejściowego skrot jest zawsze tej samej dlugosci,
    def test_hash_length(self):
        hash_length = -1
        for _ in range(self.n):
            random_string = ''.join(secrets.choice(string.ascii_uppercase + string.digits) for _ in range(random.randint(10, 100)))
            generated_hash = SHA256.encode(random_string)
            self.assertFalse(len(generated_hash) != hash_length and hash_length != -1)
            hash_length = len(generated_hash)
        print('Dlugosc hasha: ', hash_length)
        print()

    # 2. Czy jesli zmienimy 1 bit w wiadomosci skracanej, to zmieni sie wiekszosc ( >50%) bitow skrotu,
    def test_keys_tuple_length(self):
        global_percent_bits = 0
        global_percent_bytes = 0
        for _ in range(self.n):
            random_string = ''.join(secrets.choice(string.ascii_uppercase + string.digits) for _ in range(random.randint(10, 100)))
            generated_hash = SHA256.encode(random_string)
            bit_array = BitArray.to_bits(random_string[random.randint(0, len(random_string) - 1)], 8)
            random_bit_number = random.randint(0, len(bit_array) - 1)
            bit_array[random_bit_number] = abs(bit_array[random_bit_number] - 1)
            random_string = BitArray.from_bits(bit_array, 8)
            new_generated_hash = SHA256.encode(random_string)
            generated_hash_bits = BitArray.to_bits(generated_hash, 8)
            new_generated_hash_bits = BitArray.to_bits(new_generated_hash, 8)
            counter_bits = 0
            counter_bytes = 0
            self.assertTrue(len(generated_hash_bits) == len(new_generated_hash_bits))
            for i in range(len(generated_hash_bits)):
                if generated_hash_bits[i] != new_generated_hash_bits[i]:
                    counter_bits += 1
            for i in range(len(generated_hash)):
                if generated_hash[i] != new_generated_hash[i]:
                    counter_bytes += 1
            global_percent_bits += counter_bits / len(generated_hash_bits)
            global_percent_bytes += counter_bytes / len(generated_hash)
        global_percent_bits = round(global_percent_bits * 100 / self.n, 2)
        global_percent_bytes = round(global_percent_bytes * 100 / self.n, 2)
        print('Przedzial (90, 100)')
        print(global_percent_bytes, ' % srednio zmienionych bajtow w skrocie po zmianie 1 bitu w losowym tekscie')
        print('Przedzial (30, 100)')
        print(global_percent_bits, ' % srednio zmienionych bitow w skrocie po zmianie 1 bitu w losowym tekscie')
        print()
        self.assertTrue(global_percent_bytes > 90.0)
        self.assertTrue(global_percent_bits > 30.0)
    
    # 3. Czy zachodzi wlasnosc losowej wyroczni, tzn czy dla probek wejsciowych
    # otrzymujemy skroty rozrzucone po przestrzeni wyjsciowej w sposob losowy
    def test_random_oracle(self):
        global_percent_bytes = 0
        global_percent_bits = 0
        random_bytes = [{}] * 512
        random_bits = [0] * 512
        for _ in range(self.n):
            random_string = ''.join(secrets.choice(string.digits + string.ascii_letters + string.whitespace) for _ in range(random.randint(10, 100)))
            generated_hash = SHA256.encode(random_string)
            generated_hash_bits = BitArray.to_bits(generated_hash, 8)
            for i in range(len(generated_hash_bits)):
                random_bits[i] += generated_hash_bits[i]
            for i in range(len(generated_hash)):
                if generated_hash[i] in random_bytes[i].keys():
                    random_bytes[i][generated_hash[i]] += 1
                else:
                    random_bytes[i][generated_hash[i]] = 1
        for bit in random_bits:
            global_percent_bits += bit / self.n
        for i in range(len(random_bytes)):
            mean_bytes = sum(random_bytes[i].values()) / len(random_bytes[i].values())
            global_percent_bytes += self.stdev(random_bytes[i].values()) / mean_bytes
        global_percent_bytes = round(global_percent_bytes * 100 / len(random_bytes), 2)
        print('Przedzial (0, 5)')
        print(global_percent_bytes, ' % odchylenia standardowego do sredniej zmienionych bajtow w skrocie dla losowego tekstu')
        global_percent_bits = round(global_percent_bits * 100 / len(random_bits), 2)
        print('Przedzial (40,60)')
        print(global_percent_bits, ' % srednio zmienionych bitow w skrocie dla losowego tekstu')
        print()
        self.assertTrue(global_percent_bytes < 5.0)
        self.assertTrue(40.0 < global_percent_bits < 60.0)

    @staticmethod
    def variance(data, ddof=1):
        n = len(data)
        mean = sum(data) / n
        return sum((x - mean) ** 2 for x in data) / (n - ddof)

    def stdev(self, data):
        var = self.variance(data)
        std_dev = math.sqrt(var)
        return std_dev
