import os
import sys
import unittest2

from lab3.rsa_cipher import RSA


class RSACipherTest(unittest2.TestCase):
    def test_crypto(self):
        rsa = RSA(768)
        with open(os.path.join(sys.path[0], 'test/test_lab3'), 'r', encoding='utf-8') as file:
            plain_text = file.read()
            encrypted = rsa.encode(plain_text)
            decrypted = rsa.decode(encrypted)
            self.assertEqual(plain_text, ''.join(decrypted))

    def test_keys_tuple_length(self):
        rsa = RSA(768)
        self.assertTrue(3 == len(rsa.get_key()))
