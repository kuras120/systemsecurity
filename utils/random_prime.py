import random


class RandomPrime:
    @staticmethod
    def generate_prime(digits: int, storage_size: int) -> int:
        primes = []
        size = 0
        for number in range(pow(10, digits - 1), pow(10, digits) - 1):
            if RandomPrime.is_prime(number):
                size += 1
                primes.append(number)
            if size >= storage_size:
                break
        return primes[random.randint(0, storage_size - 1)]

    @staticmethod
    def is_prime(n: int) -> bool:
        if n <= 3:
            return n > 1
        if n % 2 == 0 or n % 3 == 0:
            return False
        i = 5
        while i ** 2 <= n:
            if n % i == 0 or n % (i + 2) == 0:
                return False
            i += 6
        return True
