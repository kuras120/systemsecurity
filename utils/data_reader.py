

class DataReader:
    @staticmethod
    def read_sha_data(file_path):
        parsed_values = []
        with open(file_path, 'r', encoding='utf-8') as file:
            for line in file.readlines():
                for value in line.split():
                    parsed_values.append(value.replace('0x', '').replace(',', ''))
        return parsed_values
