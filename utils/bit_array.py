class BitArray:
    @staticmethod
    def to_bits(s, bits_per_byte):
        result = []
        for c in s:
            bits = bin(ord(c))[2:]
            bits_string = bits_per_byte * '0'
            bits = bits_string[len(bits):] + bits
            result.extend([int(b) for b in bits])
        return result

    @staticmethod
    def from_bits(bits, bits_per_byte) -> str:
        chars = []
        for b in range(int(len(bits) / bits_per_byte)):
            byte = bits[b*bits_per_byte:(b+1)*bits_per_byte]
            chars.append(chr(int(''.join([str(bit) for bit in byte]), 2)))
        return ''.join(chars)
