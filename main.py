import os
import readchar

import lab2.executable
import lab3.executable
import lab4.executable

if __name__ == '__main__':
    while True:
        os.system('cls' if os.name == 'nt' else 'clear')
        project = input('Choose project:\n1 - Vernam Cipher\n2 - RSA\n3 - SHA256\nq - Exit\n')
        print()
        if project == '1':
            lab2.executable.run()
        elif project == '2':
            lab3.executable.run()
        elif project == '3':
            lab4.executable.run()
        elif project == 'q':
            break
        else:
            print('Not valid operation')
        print('\nPress any key to continue . . . ')
        try:
            readchar.readchar()
        except Exception:
            print('IDE env not supported - please run in terminal or terminal emulation mode\n')
            raise Exception
