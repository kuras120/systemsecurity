# SystemSecurity

Ciphers and other security things

1. Aplikacja uruchamiana z wykorzystaniem interpretera pythona.
2. Do aplikacji dodany skrypt pod Windows OS, uruchamiajacy dodatkowo testy 
   i aplikacje w dostarczonym, izolowanym srodowisku (start.bat).
3. W celu samodzielnego uruchomienia aplikacji nalezy wykonac komende:
   - pip install -r requirements.txt (instalacja zaleznosci)
   - python main.py (uruchomienie aplikacji),
   - python -m unittest test/test_lab[nr].py (uruchomienie testu dla danego projektu).
4. W przypadku projektu 4 (funkcja hashujaca), zadanie zostalo 
   w calosci wykonane za pomoca testow jednostkowych.

---
Opis projektow:
1. Szyfr Vernama dziala prawidlowo, testy wskazuja na poprawna implementacje.
2. Szyfr RSA wykonuje sie poprawnie, bloki 10 znakowe umieszczone 
   sa w algorytmie, jednak na standardowe wyjscie przekazywany jest 
   odszyfrowany tekst (nastepuje zlaczenie blokow).
3. Do wygenerowania skrotu wykorzystana zostala biblioteka hashlib, 
   przetestowana za pomoca testow jednostkowych:
    - Skrot zawiera zawsze taka sama liczbe znakow,
    - W przypadku podobnego tekstu, zostaje wygenerowana zupelnie rozna 
      funkcja skrotu(90% roznicy miedzy znakami, 30% miedzy bitami),
    - W przypadku losowego tekstu, skrot zawiera dosc ograniczona liczbe 
      znakow, natomiast ich polozenie rozni sie. Wyliczone zostalo odchylenie 
      standardowe, ktore w stosunku do sredniej jest bardzo male co pozwala 
      na wnioskowanie, iz wsrod N powtorzen, kazdy hash jest zupelnie inny 
      (1.5% stosunku odchylenia do sredniej, 43% roznicy miedzy bitami).
    - Kazdy test zostal powtorzony 1000 razy dla usrednienia wynikow.  