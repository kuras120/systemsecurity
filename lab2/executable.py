from lab2.vernam_cipher import Vernam


def run():
    file_path = input('Enter file path (e.g. test/test_lab2): ')
    vernam = Vernam()
    with open(file_path, 'r', encoding='utf-8') as file:
        plain_text = file.read()
        print('\nPlain text: ', plain_text)
        encrypted = vernam.encode(plain_text)
        print('Encrypted value: ', encrypted.encode('utf-8', 'replace').decode())
        decrypted = vernam.decode(encrypted)
        print('Decrypted value: ', decrypted)
