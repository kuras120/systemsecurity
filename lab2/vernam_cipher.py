import random
from utils.bit_array import BitArray
from utils.random_prime import RandomPrime


class Vernam:
    def __init__(self):
        self.__key = []

    def encode(self, text: str, key: str = '') -> str:
        if len(text) != len(key):
            self.__key = self.__generate_key(
                len(text) * 16,
                RandomPrime.generate_prime(10, 10),
                RandomPrime.generate_prime(10, 10)
            )
        else:
            self.__key = key
        return self.__generate_crypto(text)

    def decode(self, crypto: str) -> str:
        return self.__reverse_crypto(crypto)

    def get_key(self):
        return self.__key

    def __generate_key(self, size: int, p: int, q: int):
        n = p * q
        s = random.randint(1, n - 1)
        temp_key = [(s * s) % n]
        for i in range(1, size):
            temp_key.append((temp_key[i-1] * temp_key[i - 1]) % n)
        self.__key = []
        for i in range(size):
            self.__key.append(temp_key[i] % 2)
        return self.__key

    def __generate_crypto(self, text: str) -> str:
        bit_text = BitArray.to_bits(text, 16)
        crypto = []
        for i in range(len(bit_text)):
            crypto.append((bit_text[i] + self.__key[i]) % 2)
        return BitArray.from_bits(crypto, 16)

    def __reverse_crypto(self, crypto: str) -> str:
        bit_text = BitArray.to_bits(crypto, 16)
        text = []
        for i in range(len(bit_text)):
            text.append((bit_text[i] - self.__key[i]) % 2)
        return BitArray.from_bits(text, 16)
